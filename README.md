# Exercício - GPIO - Entradas e Saídas Digitais

Exercício de acionamento das Entradas e Saídas Digitais (GPIO) utilizando a Raspberry Pi em Laboratório remoto.

## 1 - OBJETIVOS

Este exercício tem o objetivo de efetuar o controle das **entradas** e **saídas** digitais (GPIO).

A escrita nas **saídas** serão realizadas em modo ***liga/desliga***.

A leitura das **entradas** serão realizadas em modo ***pooling***, ***eventos*** e ***interrupções***. 

Os exercícios podem ser realizados tanto em linguagem ***Python*** quanto em ***C/C++***.

Os pinos da GPIO da Raspberry Pi tem funções de propósito geral (Entrada / Saída) ou propósitos específicos de acordo com o protocolo de comunicação desejado.

A figura abaixo ilustra a designação padrão de cada pino. Além dos pinos de GPIO, há pinos com propósitos específicos como:

- **3v3 Power** que fornece tensão de 3.3V;  
- **5v Power** que fornece tensão de 5V;
- **Ground** ou **GND** que que é a referência de tensão ou tensão 0V.       

![Figura RPI3 GPIO](imagens/Rpi3_GPIO_Mapa.png)   
Referência: https://pinout.xyz/

## 2 - Circuito Remoto

O sistema já está montado em um Laboratório Remoto da Faculdade do Gama (FGA) acessível através do link [Dashboard Remoto](http://164.41.98.25:443/dashboard/1f2e6190-de1b-11ed-abfa-f3ea15799da1?publicId=ba042a80-0322-11ed-9f25-414fbaf2b065). 

![Dashboard](imagens/Imagem_Dashboard.png)

A configuração disponível possui as seguintes entradas e saídas disponíveis.

| Item                                              | Direção |
|---------------------------------------------------|:-------:|
| GPIO 05                                           | Saída   |
| GPIO 06                                           | Saída   |
| GPIO 13                                           | Saída   |
| GPIO 19                                           | Saída   |
| GPIO 26                                           | Saída   |
| GPIO  0                                           | Entrada |
| GPIO 09                                           | Entrada |
| GPIO 10                                           | Entrada |
| GPIO 11                                           | Entrada |
| GPIO 27                                           | Entrada |

### Acesso

O sistema utiliza uma Raspberry Pi 4 e o acesso para controle da GPIO deve ser realizado através do protocolo SSH da seguinte maneira:

```
ssh <usuario>@164.41.98.28 -p 13508
```

O **usuário** de cada aluno é composto pela combinação de seu primeiro e último nomes em minusculo e sem acento. A **senha** é a sua matrícula.

## 4 - Exercícios 

### Exercício 1 - Liga/Desliga

Neste exercício a porta GPIO deve ser ativada como saída digital.

A) Acionar o LED ligando e desligando;  
B) Acionar o LED em modo liga/desliga temporizado.
C) Acionar os LEDs em sequência ativando um padrão temporizado.

### Exercício 2 - Entradas em Polling

A) Utilizar a técnica de polling para ler o estado de um botão (ou jumper) e imprimir na tela cada mudança de estado;  
B) Utilizar a técnica de debounce para filtrar as mudanças de estado espúrias do botão;  

### Exercício 3 - Eventos e Interrupções

A) Utilizar o recurso de monitorar eventos para ler o estado de um botão;  
B) Utilizar as interrupções para monitorar a mudança de estado de um botão;  
C) Utilizando eventos ou interrupções, tratar o problema de bouncing para ler o estado de uma entrada.

## 5 - Referências

### Bibliotecas em Python

- gpiozero (https://gpiozero.readthedocs.io)
- RPi.GPIO (https://pypi.org/project/RPi.GPIO/)

A documentação da RPi.GPIO se encontra em
https://sourceforge.net/p/raspberry-gpio-python/wiki/Examples/

### Bibliotecas em C/C++

- WiringPi (http://wiringpi.com/)
- BCM2835 (http://www.airspayce.com/mikem/bcm2835/)
- PiGPIO (http://abyz.me.uk/rpi/pigpio/index.html)
- sysfs (https://elinux.org/RPi_GPIO_Code_Samples)

### Lista de Exemplos

Há um compilado de exemplos de acesso à GPIO em várias linguages de programação como C, C#, Ruby, Perl, Python, Java e Shell (https://elinux.org/RPi_GPIO_Code_Samples).